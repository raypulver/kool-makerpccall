'use strict';

module.exports = (XMLHttpRequest) => {
  let id = 0;
  return (rpc, method, params = []) => rpc.send ? new Promise((resolve, reject) => rpc.send({
		jsonrpc: '2.0',
		id: id++,
		method,
		params
	}, (err, response) => {
		if (err) reject(err);
		const {
			result,
			error
		} = response;
		if (!error) return resolve(result);
		const rejectWith = new Error(error.message);
		rejectWith.code = error.code;
		reject(rejectWith);
	})) : new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest();
    xhr.addEventListener('error', () => reject(new Error('XMLHttpRequest error')));
    xhr.addEventListener('readystatechange', () => {
      try {
        if (xhr.readyState === 4) {
			  	if (xhr.status !== 200) {
			  		const rejectWith = new Error('RPC responded with status code ' + String(xhr.status) + ' and response: ' + xhr.responseText);
			  		rejectWith.responseText = xhr.responseText;
			  		rejectWith.status = xhr.status;
			  		reject(rejectWith);
  		  	} else {
            const {
              result,
              error
            } = JSON.parse(xhr.responseText);
            if (error) {
              const rejectWith = new Error(error.message);
              rejectWith.code = error.code;
              reject(rejectWith);
            }
            resolve(result);
  		    }
        }
      } catch (e) {
		  	const rejectWith = new Error('Invalid response from RPC: ' + e.responseText);
		  	rejectWith.responseText = xhr.responseText;
				return reject(rejectWith);
      }
    });
    xhr.open('POST', typeof rpc === 'object' ? rpc.url : rpc);
    xhr.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
		if (typeof rpc === 'object' && Array.isArray(rpc.headers)) rpc.headers.forEach(([ header, data ]) => xhr.setRequestHeader(header, data));
    try {
      xhr.send(JSON.stringify({
        jsonrpc: '2.0',
        id: id++,
        method,
        params
      }));
    } catch (e) {
      reject(e);
    }
	});
};
