'use strict';

const koolRpcCall = require('../');
const { expect } = require('chai');

describe('kool rpc call', () => {
  it('it should make an rpc call', async () => {
    const balance = await koolRpcCall('https://mainnet.infura.io/mew', 'eth_getBalance', [ '0x0000000000000000000000000000000000000000', 'latest' ]);
    expect(balance).to.be.a.string;
  });
  it('it should catch errors', async () => {
    try {
      await koolRpcCall('https://mainnet.infura.io/mew', 'eth_getBalance', [ '0x000000000000000000000000000000000000000', 'latest' ]);
      expect(false).to.be.true;
    } catch (e) {
      expect(e).to.be.an('error');
    }
  });
	it('should provide info when a host is down', async () => {
		try {
      await koolRpcCall('http://0x0000000000000000000000000000000000000001.com', 'eth_getBalance', [ '0x000000000000000000000000000000000000000', 'latest' ]);
			expect(false).to.be.true;
		} catch (e) {
			expect(e.status).to.eql(0);
		}
	});
});
